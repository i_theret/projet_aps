open Ast;;

type valeur = 
	InVal of int 
	| InF of expr * string list * env list 
	| InFR of expr * string * string list * env list 
	| InAdr of int
	| InP of block * string list * env list
	| InPRec of block * string * string list * env list
and 
	env = Couple of string * valeur 
type memoire = 
	Mem of int * valeur ref

(*ident*)
let rec in_env ident l =
	match l with
	[] -> false
	| Couple(str, _)::tl ->  if (String.equal str ident) then true else (in_env ident tl)
let rec chercher_env ident l =
	match l with
	| Couple(str, v)::tl ->  if (String.equal str ident) then v else (chercher_env ident tl)
	| _ -> failwith "error chercher_env"


let get_InVal v = 
	match v with
	| InVal(n) -> n
	| _ -> failwith "error get_InVal"

(*memory*)
let rec in_mem adr l =
	match l with
	[] -> false
	| Mem(a, _)::tl ->  if (a = adr) then true else (in_mem adr tl)
	
let rec chercher_mem adr l =
	match l with
	| Mem(a, v)::tl ->  if (a = adr) then v else (chercher_mem adr tl)
	| _ -> failwith "error chercher_mem"




(*Unary*)
let prim1 op v =
	match op with
	| NOT -> if (get_InVal v) == 0 then InVal(1)
		else if (get_InVal v) == 1 then InVal(0)
		else failwith "error prim1"
(*Binary*)
let prim2 op v1 v2 =
	let inVal_v1 = (get_InVal v1) 
	and inVal_v2 = (get_InVal v2)
	in match op with
	| EQ -> if (inVal_v1 == inVal_v2) then InVal(1) else InVal(0)
	| LT -> if (inVal_v1 < inVal_v2) then InVal(1) else InVal(0)
	| ADD -> InVal(inVal_v1 + inVal_v2)
	| SUB -> InVal(inVal_v1 - inVal_v2)
	| MUL -> InVal(inVal_v1 * inVal_v2)
	| DIV -> InVal(inVal_v1 / inVal_v2)


(* construit liste argument *)
let rec get_args l = 
	match l with
  	[] -> []
	|ASTArg(str, _)::tl -> str::(get_args tl)
let rec get_argsp l = 
	match l with
  	   [] -> []
	|ASTArgp(str, _)::tl -> str::(get_argsp tl)
	|ASTArgpVar(str, _)::tl -> str::(get_argsp tl)


(*calcul expression*)
let rec calcul_expr expr env mem =
    match expr with
    | ASTNum(n) -> InVal(n)    
    | ASTId(x) -> if (in_env x env) then 
                        match (chercher_env x env) with
                        | InAdr(a) -> if (in_mem a mem) then 
                                    !(chercher_mem a mem)
                                    else failwith "error No adress"
                        | v -> v
                 else failwith "ident n'existe pas"
    | ASTBool(b) -> if b then InVal(1) else InVal(0)
    | ASTUnary(x, e) -> prim1 x (calcul_expr e env mem) 
    | ASTBinary(x, e1, e2) -> prim2 x (calcul_expr e1 env mem)  (calcul_expr e2 env mem)
    | ASTIf(e1, e2, e3) -> if (calcul_expr e1 env mem) = InVal(1) then (calcul_expr e2 env mem)
                            else (calcul_expr e3 env mem)
    | ASTAnd(e1, e2) -> if (calcul_expr e1 env mem) == InVal(0) then InVal(0) 
                            else (calcul_expr e2 env mem) 
    | ASTOr(e1, e2) -> if (calcul_expr e1 env mem) == InVal(1) then InVal(1) 
                            else (calcul_expr e2 env mem) 
    | ASTArgs(l, e) -> InF(e,get_args(l), env)
    | ASTApp(e, el) -> match (calcul_expr e env mem) with
                    | InF(e2, args, env2) ->
                        let new_env = env2@(assoc_arg_val args el env mem) in
                        calcul_expr e2 new_env mem
                    | InFR(e2, f, args, env2) ->
                        let new_env = env2@(assoc_arg_val args el env mem)@
                        [Couple(f, InFR(e2, f, args, env2))] in
                        calcul_expr e2 new_env mem
                    | _ -> failwith "error calcul_expr ASTApp"
and assoc_arg_val args el env mem = 
	match args,el with
	| [],[] -> []
	| arg::atl, e::etl -> Couple(arg, calcul_expr e env mem)::(assoc_arg_val atl etl env mem) 
	| _ -> failwith "error assoc_arg_val"


let calcul_expr_p ep env mem =
	match ep with 
	| ASTExprp(e) -> calcul_expr e env mem
	| ASTExprpAdr(e)-> 
		begin
		match e with  
		| ASTId(x) -> 
			if (in_env x env) then 
			match (chercher_env x env) with
			| InAdr(a) -> InAdr(a)
			| v -> v
                    	else failwith "error calcul_expr_p ASTExprpAdr ASTId"
        	| _ -> failwith "error calcul_expr_p ASTExprpAdr" 
        	end

let rec assoc_arg_val_p args el env mem = 
	match args,el with
	| [],[] -> []
	| arg::atl, e::etl -> Couple(arg, calcul_expr_p e env mem)::(assoc_arg_val_p atl etl env mem) 
	| _ -> failwith "error assoc_arg_val_p"




let alloc_indice = 
	ref 0

let alloc mem = 
	let res = (!alloc_indice, Mem(!alloc_indice, ref (InVal(-1)))::mem) in
	alloc_indice := !alloc_indice + 1;
	res

let rec calcul_def expr env mem =
	match expr with
	| ASTConst(str, _, e) -> (Couple(str, (calcul_expr e env mem))::env, mem)
	| ASTFun(str, _, args, e) -> (Couple(str, InF(e, (get_args args), env))::env, mem)
	| ASTFunRec(str, _, args, e) -> (Couple(str, InFR(e, str, (get_args args), env))::env, mem)
	| ASTVar(str, _) -> let (adr, mem_prime) = (alloc mem) in (Couple(str, InAdr(adr))::env, mem_prime)
	| ASTProc(str, args, bk) -> (Couple(str, InP(bk, (get_argsp args), env))::env, mem)
	| ASTProcRec(str, args, bk) -> (Couple(str, InPRec(bk, str, (get_argsp args), env))::env, mem)



let rec calcul_cmds cmds env mem flux = 
	match cmds with
	| [ASTStat(stat)] -> calcul_instr stat env mem flux
	| ASTDec(def)::tl -> let (new_env, new_mem) = calcul_def def env mem in
                        calcul_cmds tl new_env new_mem flux
	| ASTStat(stat)::tl -> let (new_mem, new_flux) = calcul_instr stat env mem flux in
                        calcul_cmds tl env new_mem new_flux
	| _ -> failwith "error calcul_cmds"
	
and calcul_block cmds env mem flux =
	match cmds with
	| ASTBlock(cs) -> calcul_cmds cs env mem flux
	
and calcul_instr stat env mem flux =
	match stat with
	| ASTEcho(e) -> (mem, get_InVal(calcul_expr e env mem)::flux)
	| ASTSet(x, e) ->  
		if (in_env x env) then 
			begin
			match (chercher_env x env) with
			| InAdr(adr) -> 
				let v = calcul_expr e env mem in
				if (in_mem adr mem) then 
					begin
					let v_pre = chercher_mem adr mem in
					v_pre := v;
					(mem,flux)
					end 
				else failwith "adresse n'existe pas"
			| _ -> failwith "wrong valeur"
			end
		else failwith "ident n'existe pas"
	| ASTWhile(e, b) -> 
    		if (calcul_expr e env mem) = InVal(0) then (mem,flux)
		else 
			begin
			let (mem1,flux1) = calcul_block b env mem flux in
			calcul_instr stat env mem1 flux1
			end
	| ASTIfb(e, b1, b2) -> 
		if (calcul_expr e env mem) = InVal(1) then (calcul_block b1 env mem flux) 
		else (calcul_block b2 env mem flux) 
	| ASTCall(e, el) -> 
		begin
		match (calcul_expr e env mem) with
		| InP(bk, args, env2) ->
			let new_env = env2@(assoc_arg_val_p args el env mem) in
			calcul_block bk new_env mem flux
		| InPRec(bk, p, args, env2) ->
			let new_env = env2@(assoc_arg_val_p args el env mem)@
			[Couple(p, InPRec(bk, p, args, env2))] in
			calcul_block bk new_env mem flux
		| _ -> failwith "error calcul_expr ASTCall"
                    end

let rec print_output output =
	List.iter (function x -> Printf.printf "%d\n" x) (List.rev output) 


let calcul_prog cs = 
	let (mem, flux) = (calcul_cmds cs [] [] []) in
                    print_output flux;;

let fname = Sys.argv.(1) in
	let ic = open_in fname in
		try
		let lexbuf = Lexing.from_channel ic in
		let p = Parser.prog Lexer.token lexbuf in
			calcul_prog p;
			print_string "\n"
		with Lexer.Eof ->exit 0
		
		
		
		
		
